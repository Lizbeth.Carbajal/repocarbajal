const productos = [{
    id: 123,
    name: "John",
    city: "Melbourne",
    phoneNo: "1234567890",
},
{
    id: 124,
    name: "Amelia",
    city: "Sydney",
    phoneNo: "1234567890",
},
{
    id: 125,
    name: "Emily",
    city: "Perth",
    phoneNo: "1234567890",
},
{
    id: 126,
    name: "Abraham",
    city: "Perth",
    phoneNo: "1234567890",
},
];

// Ahora dibujamos la tabla
const $cuerpoTabla = document.querySelector("#cuerpoTabla");
// Recorrer todos los productos
productos.forEach(producto => {
// Crear un <tr>
const $tr = document.createElement("tr");
 // El td del personID
 let $tdpersonID = document.createElement("td");
 $tdpersonID.textContent = producto.id;
 $tr.appendChild($tdpersonID);
// Creamos el <td> de name y lo adjuntamos a tr
let $tdname = document.createElement("td");
$tdname.textContent = producto.name; // el textContent del td es el name
$tr.appendChild($tdname);
// El td de city
let $tdcity = document.createElement("td");
$tdcity.textContent = producto.city;
$tr.appendChild($tdcity);
// El td del phoneNo
let $tdphoneNo = document.createElement("td");
$tdphoneNo.textContent = producto.phoneNo;
$tr.appendChild($tdphoneNo);

// Finalmente agregamos el <tr> al cuerpo de la tabla
$cuerpoTabla.appendChild($tr);
// Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
});